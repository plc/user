# Sentinel

A class to authenticate an MSU user using Sentinel.

---
## Examples

```php
<?php

use Plc\User\Security\Sentinel\Sentinel;

$sentinel = new Sentinel();

$sentinel->setEnvironment(Sentinel::ENV_DEV)
  ->setAppId('TEST_APP')
  ->setClientId('TESTCLIENTID')
  ->setClientSecret('testclientpassword);

if (true === $sentinel->authenticate()) {
  $userData = $sentinel->getUserData();
}
```

### Return types

By default, ```getUserData()``` returns an array, however an object may be returned. This object may be any class that implements *Plc\User\Security\Sentinel\User\UserInterface*. If a class is not specified through ```setUserClass($userClass)```, *Plc\User\Security\Sentinel\User\User* will be instantiated.

```php
<?php

use Plc\User\Security\Sentinel\Sentinel;

$sentinel = new Sentinel();

$sentinel->setEnvironment(Sentinel::ENV_DEV)
  ->setAppId('TEST_APP')
  ->setClientId('TESTCLIENTID')
  ->setClientSecret('testclientpassword)
  ->setReturnType(Sentinel::RETURN_OBJECT);

if (true === $sentinel->authenticate()) {
  $user = $sentinel->getUserData();
}
```