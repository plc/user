<?php

namespace Plc\User\Security\Sentinel;

use Plc\User\Security\Sentinel\User\UserInterface;

class Sentinel
{
    /**
     * @const int Signify that this class is being used in a development tier.
     */
    const ENV_DEV  = 0;

    /**
     * @const int Signify that this class is being used in a quality assurance tier.
     */
    const ENV_QUAL = 1;

    /**
     * @const int Signify that this class is being used in a production tier.
     */
    const ENV_PROD = 2;
    /**
     * @const int Return user data as an array
     */
    const RETURN_ARRAY  = 0;
    /**
     * @const int Return user data as an object.
     */
    const RETURN_OBJECT = 1;


    /**
     * @var string The application ID
     */
    protected $appId;
    /**
     * @var string client ID
     */
    protected $clientId;
    /**
     * @var string The client password
     */
    protected $clientSecret;
    /**
     * @var string The URL to redirect the user to in order to input credentials
     */
    protected $loginUrl     = 'https://login.msu.edu';
    /**
     * @var string The URL that acts as the target namespace for the SOAP service
     */
    protected $namespaceUrl = 'https://login.msu.edu';
    /**
     * @var int Determine whether or not to return the user data as an array (default) or an object.
     */
    protected $returnType   = self::RETURN_ARRAY;
    /**
     * @var string The class to instantiate if the return type is set to RETURN_OBJECT
     */
    protected $userClass = '\Plc\User\Security\Sentinel\User\User';
    /**
     * @var array User data returned from Sentinel.
     */
    protected $userData     = array();
    /**
     * @var string The URL location of Sentinel's WSDL file
     */
    protected $wsdlUrl      = 'https://login.msu.edu';
    /**
     * @var string The URL location to return a user to
     */
    protected $redirectUrl;


    /**
     * Initialize the object.
     */
    public function __construct()
    {
        $this->userData = array(
            'aPid'        => null,
            'email'       => null,
            'fName'       => null,
            'lName'       => null,
            'netId'       => null,
            'roles'       => null,
            'securityIds' => null,
            'ssn'         => null,
            'uuId'        => null,
            'zPid'        => null,
        );
    }


    /**
     * A wrapper to provide simple, turn-key authentication.
     *
     * @return bool True if the user is authenticated, false otherwise.
     */
    public function authenticate()
    {
        // Check for an access token and if one does not exist then request one.
        // If an access token is present in the query string then attempt
        // authentication.
        if (false === isset($_GET['Token'])) {
            $this->fetchToken();
        }

        return $this->validateToken($_GET['Token']);
    }


    /**
     * Sends the user to MSU's OAuth authorization server so the user can sign
     * in. The user will be redirected back to your application with an
     * authorization code in the query string.
     */
    public function fetchToken()
    {
        $query = array(
            'App' => $this->appId,
        );
        if (false === empty($this->redirectUrl)) {
            $query['service'] = $this->redirectUrl;
        }

        header('Location: ' . $this->loginUrl . '?' . http_build_query($query, null, '&'));
    }


    /**
     * Return the member variable $appId.
     *
     * @return string
     */
    public function getAppId()
    {
        return $this->appId;
    }


    /**
     * Return the member variable $clientId.
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }


    /**
     * Return the member variable $clientSecret.
     *
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }


    /**
     * Return the member variable $loginUrl.
     *
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }


    /**
     * Return the member variable $namespaceUrl.
     *
     * @return string
     */
    public function getNamespaceUrl()
    {
        return $this->namespaceUrl;
    }

    /**
     * Return the member variable $userClass.
     *
     * @return string
     */
    public function getUserClass()
    {
        return $this->userClass;
    }


    /**
     * Return the member variable $userData.
     *
     * @return array
     */
    public function getUserData()
    {
        if (self::RETURN_OBJECT === $this->returnType) {
            $user = new $this->userClass;
            $user->mapFromArray($this->userData);
            return $user;
        }

        return $this->userData;
    }


    /**
     * Return the member variable $wsdlUrl.
     *
     * @return string
     */
    public function getWsdlUrl()
    {
        return $this->wsdlUrl;
    }


    /**
     * Return the member variable $redirectUrl.
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }


    /**
     * Takes an array supplied by validateToke() and maps it to the member
     * variable $userData.
     *
     * @param array $userData
     */
    protected function mapUserData($userData)
    {
        foreach ($userData as $key => $value) {
            $this->userData[$key] = $value;
        }
    }

    /**
     * Set the member variable $appId.
     *
     * @param  string $appId
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
        return $this;
    }


    /**
     * Set the member variable $clientId.
     *
     * @param  string $clientId
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }


    /**
     * Set the member variable $clientSecret.
     *
     * @param  string $clientSecret
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }


    /**
     * A helper method to configure the URLs based on the development tier.
     *
     * @param  int $environment
     * @return \Plc\User\Security\Sentinel\Sentinel
     * @throws Exception
     */
    public function setEnvironment($environment)
    {
        if (self::ENV_DEV === $environment) {
            $this->loginUrl     = 'https://login.dev.ais.msu.edu/';
            $this->namespaceUrl = 'http://login.dev.ais.msu.edu/type';
            $this->wsdlUrl      = 'http://login.dev.ais.msu.edu/service/clientv4?wsdl';

        } elseif (self::ENV_QUAL === $environment) {
            $this->loginUrl     = 'https://login.qual.ais.msu.edu/';
            $this->namespaceUrl = 'http://login.qual.ais.msu.edu/type';
            $this->wsdlUrl      = 'http://login.qual.ais.msu.edu/service/clientv4?wsdl';

        } elseif (self::ENV_PROD === $environment) {
            $this->loginUrl     = 'https://login.msu.edu/';
            $this->namespaceUrl = 'http://login.msu.edu/type';
            $this->wsdlUrl      = 'http://login.msu.edu/service/clientv4?wsdl';

        } else {
            throw new Exception('Unrecognized environment: ' . $environment);
        }

        return $this;
    }


    /**
     * Set the member variable $loginUrl.
     *
     * @param  string $loginUrl
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setLoginUrl($loginUrl)
    {
        $this->loginUrl = $loginUrl;
        return $this;
    }


    /**
     * Set the member variable $namespaceUrl.
     *
     * @param  string $namespaceUrl
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setNamespaceUrl($namespaceUrl)
    {
        $this->namespaceUrl = $namespaceUrl;
        return $this;
    }


    /**
     * Sets the member variable $returnType.
     *
     * @param  $returnType
     */
    public function setReturnType($returnType)
    {
        $returnTypes = array(
            self::RETURN_ARRAY,
            self::RETURN_OBJECT
        );

        if (false === in_array($returnType, $returnTypes)) {
            throw new \Exception('Invalid return type');
        }

        $this->returnType = $returnType;

        return $this;
    }


    /**
     * Set the user class to use when RETURN_OBJECT is specified as the return
     * type.
     *
     * @param  string $userClass
     * @throws \Exception
     */
    public function setUserClass($userClass)
    {
        $testClass = new $userClass;

        if (false === $testClass instanceof UserInterface) {
            throw new \Exception('User class "' . $userClass . '" does not implement Plc\User\Security\Sentinel\User\UserInterface');
        }

        $this->userClass = $userClass;
    }


    /**
     * Set the member variable $wsdlUrl.
     *
     * @param  string $wsdlUrl
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setWsdlUrl($wsdlUrl)
    {
        $this->wsdlUrl = $wsdlUrl;
        return $this;
    }


    /**
     * Set the member variable $redirectUrl.
     *
     * @param  string $redirectUrl
     * @return \Plc\User\Security\Sentinel\Sentinel
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }


    /**
     * Issue a SOAP request to validate the token passed in via URL by Sentinel.
     *
     * @param string $token
     */
    protected function validateToken($token)
    {
        $clientOptions  = array(
            'style' => SOAP_DOCUMENT,
            'use'   => SOAP_LITERAL,
            'trace' => true,
        );

        $client = new \SoapClient($this->wsdlUrl, $clientOptions);

        try {

            $methodOptions = array(
                '{' . $this->namespaceUrl . '}getCredential' => array(
                    'appID'            => $this->appId,
                    'clientUserID'     => $this->clientId,
                    'clientPassword'   => $this->clientSecret,
                    'token'            => $token,
                    'browserIPAddress' => $_SERVER['REMOTE_ADDR'],
                ),
            );

            $result = $client->__soapCall('getCredential', $methodOptions);

            if (0 === $result->status->code) {
                $userData = array(
                    'aPid'           => ($result->credential->studentPID     === 'Not Authorized') ? null    : $result->credential->studentPID,
                    'email'          => $result->credential->emailAddress,
                    'fName'          => ($result->credential->firstName      === 'Not Authorized') ? null    : $result->credential->firstName,
                    'lName'          => ($result->credential->lastName       === 'Not Authorized') ? null    : $result->credential->lastName,
                    'netId'          => ($result->credential->publicID       === 'Not Authorized') ? null    : $result->credential->publicID,
                    'roles'          => ($result->credential->roleList       === 'null')           ? array() : explode('~', $result->credential->roleList),
                    'securityIds'    => ($result->credential->securityIDList === 'null')           ? array() : explode('~', $result->credential->securityIDList),
                    'ssn'            => ($result->credential->SSN            === 'Not Authorized') ? null    : $result->credential->SSN,
                    'uuId'           => $result->credential->UUID,
                    'zPid'           => ($result->credential->employeePID    === 'Not Authorized') ? null    : $result->credential->employeePID,
                );

                $this->mapUserData($userData);

                return true;
            }

            return false;

        } catch (\SoapFault $e) {
            echo 'The following error was received: <pre>' . var_dump($e) . '</pre>';
        }

    }

}
