<?php

namespace Plc\User\Security\Sentinel\User;

use Plc\User\UserAbstract;
use Plc\User\Security\Sentinel\User\UserInterface;

class User extends UserAbstract implements UserInterface
{
    /**
     * @var string A student's PID.
     */
    protected $aPid;
    /**
     * @var array The roles assigned to the user.
     */
    protected $roles       = array();
    /**
     * @var array The security IDs assigned to the user.
     */
    protected $securityIds = array();
    /**
     * @var string The user's social security number.
     */
    protected $ssn;
    /**
     * @var string The User's UUID.
     */
    protected $uuId;
    /**
     * @var The employee's PID.
     */
    protected $zPid;


    /**
     * Return the member variable $aPid.
     *
     * @return string
     */
    public function getAPid()
    {
        return $this->aPid;
    }


    /**
     * Return the member variable $roles.
     *
     * @return string
     */
    public function getRoles()
    {
        return $this->roles;
    }


    /**
     * Return the member variable $securitIds.
     *
     * @return string
     */
    public function getSecurityIds()
    {
        return $this->securityIds;
    }


    /**
     * Return the member variable $ssn.
     *
     * @return string
     */
    public function getSsn()
    {
        return $this->ssn;
    }


    /**
     * Return the member variable $uuId.
     *
     * @return string
     */
    public function getUuId()
    {
        return $this->uuId;
    }


    /**
     * Return the member variable $zPid.
     *
     * @return string
     */
    public function getZPid()
    {
        return $this->zPid;
    }


    /**
     * Set the member variable $aPid.
     *
     * @param  string $aPid
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setAPid($aPid)
    {
        $this->aPid = $aPid;
        return $this;
    }


    /**
     * Set the member variable $roles.
     *
     * @param  array $roles
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setRoles($roles)
    {

        if (false === is_array($roles)) {
            throw new \InvalidArgumentException('"$roles" must be an array. "' . gettype($roles) . '" provided.');
        }

        $this->roles = $roles;
        return $this;
    }


    /**
     * Set the member variable $securityIds.
     *
     * @param  array $securityIds
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setSecurityIds($securityIds)
    {
        if (false === is_array($securityIds)) {
            throw new \InvalidArgumentException('"$securityIds" must be an array. "' . gettype($securityIds) . '" provided.');
        }

        $this->securityIds = $securityIds;
        return $this;
    }


    /**
     * Set the member variable $ssn.
     *
     * @param  string $ssn
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setSsn($ssn)
    {
        $this->ssn = $ssn;
        return $this;
    }


    /**
     * Set the member variable $uuId.
     *
     * @param  string $uuId
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setUuId($uuId)
    {
        $this->uuId = $uuId;
        return $this;
    }


    /**
     * Set the member variable $zPid.
     *
     * @param  string $zPid
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setZPid($zPid)
    {
        $this->zPid = $zPid;
        return $this;
    }
}
