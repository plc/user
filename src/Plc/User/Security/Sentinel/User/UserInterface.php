<?php

namespace Plc\User\Security\Sentinel\User;

interface UserInterface
{
    public function getAPid();
    public function getEmail();
    public function getFName();
    public function getLName();
    public function getNetId();
    public function getRoles();
    public function getSecurityIds();
    public function getSsn();
    public function getUuId();
    public function getZPid();
    public function mapFromArray($data);
    public function setAPid($aPid);
    public function setEmail($email);
    public function setFName($fName);
    public function setLName($lName);
    public function setNetId($netId);
    public function setRoles($roles);
    public function setSecurityIds($securityIds);
    public function setSsn($ssn);
    public function setUuId($uuId);
    public function setZPid($zPid);
}
