# MSU OAuth2

An OAuth2 authentication component for use with MSU's OAuth2 service.

## Examples

### Turn-key authentication
```
<?php

    //http://example.msu.edu/login.php

    use Plc\User\Security\OAuth2\OAuthClient;

    $clientId     = 'OAuth-Example-App';
    $clientSecret = '7zF_#w7CF2Zu_0??C3$AJq=E6qPa4NFRw46gNruX!v';
    $returnUrl    = 'https://example.msu.edu/login.php';

    $msuAuth = new OAuthClient();

    $msuAuth->setClientId($clientId)->setClientSecret($clientSecret)->setRedirectUrl($returnUrl);

    if (true === $msuAuth->authenticate()) {
        echo 'Log in successful!';
    } else {
        echo 'Uh oh, something went wrong.';
    }
?>
```


### Manual authentication
```
<?php

    //http://example.msu.edu/login.php

    use Plc\User\Security\OAuth2\OAuthClient;

    $clientId     = 'OAuth-Example-App';
    $clientSecret = '7zF_#w7CF2Zu_0??C3$AJq=E6qPa4NFRw46gNruX!v';
    $returnUrl    = 'https://example.msu.edu/login.php';

    $msuAuth = new OAuthClient();
    $msuAuth->setClientId($clientId)->setClientSecret($clientSecret)->setRedirectUrl($returnUrl);

    if (false === isset($_GET['code'])) {
           $msuAuth->fetchAuthorizationCode();
    } else {
           $response   = $msuAuth->fetchTokens();
    }

    if ($response['httpStatusCode'] == 200) {
        echo 'Log in successful! Here are your tokens and related information:';
        echo '<pre>', var_dump($response), '<pre>';
    } else {
        echo 'Uh oh, something went wrong.';
    }
?>
```

### Getting user data

```php

    if (true === $msuAuth->authenticate()) {
        $userData = $msuAuth->getUserData();
    } else {
        echo 'Uh oh, something went wrong.';
    }
```
#### Return types

*OAuth2Client* defaults to returning an array. By executing ```setReturnType()``` an object of the type *Plc\User\Security\OAuth2\User\UserInterface* will be constructed. By default, *Plc\User\Security\OAuth2\User\* is the class that will be instantiated, however this can be overridden by using ```setUserClass()``` and providing a fully qualified namespaced class.


```php

    if (true === $msuAuth->authenticate()) {
        $user = $msuAuth->setReturnType(OAuthClient::RETURN_OBJECT)->getUserData();

        echo 'My name is ' . $user->getFName() . '.';

    } else {
        echo 'Uh oh, something went wrong.';
    }
```