<?php

namespace Plc\User\Security\OAuth2\User;

use Plc\User\UserAbstract;
use Plc\User\Security\OAuth2\User\UserInterface;

class User extends UserAbstract implements UserInterface
{
    /**
     * @var string The User's UUID.
     */
    protected $uuId;


    /**
     * Return the member variable $uuId.
     *
     * @return string
     */
    public function getUuId()
    {
        return $this->uuId;
    }


    /**
     * Set the member variable $uuId.
     *
     * @param  string $uuId
     * @return \Plc\User\Security\Sentinel\User\User
     */
    public function setUuId($uuId)
    {
        $this->uuId = $uuId;
        return $this;
    }

}
