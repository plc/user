<?php

namespace Plc\User\Security\OAuth2\User;

interface UserInterface
{
    public function getEmail();
    public function getFName();
    public function getLName();
    public function getNetId();
    public function getUuId();
    public function mapFromArray($data);
    public function setEmail($email);
    public function setFName($fName);
    public function setLName($lName);
    public function setNetId($netId);
    public function setUuId($uuId);
}
