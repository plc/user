<?php

namespace Plc\User\Security\OAuth2;

use Plc\User\Security\Oauth2\User\UserInterface;

class OAuthClient
{
    /**
     * @const int Signify that this class is being used in a development tier.
     */
    const ENV_DEV  = 0;
    /**
     * @const int Signify that this class is being used in a production tier.
     */
    const ENV_PROD = 1;
    /**
     * @const int Return user data as an array
     */
    const RETURN_ARRAY  = 0;
    /**
     * @const int Return user data as an object.
     */
    const RETURN_OBJECT = 1;


    /**
     * @var string The access token provided by the MSU OAuth server.
     */
    protected $accessToken;
    /**
     * @var string The URL to authenticate the user
     */
    protected $authUrl      = 'https://oauth.itservices.msu.edu/oauth/authorize';
    /**
     * @var string Identifies the application to the MSU OAuth server
     */
    protected $clientId;
    /**
     * @var string Authenticates the application to the MSU OAuth server
     */
    protected $clientSecret;
    /**
     * @var string The URL to fetch user data from
     */
    protected $dataUrl      = 'https://oauth.itservices.msu.edu/oauth/me';
    /**
     * @var string The path to redirect the user after being authenticated.
     */
    protected $redirectUrl;
    /**
     * @var string The refresh token provided by the MSU OAuth server
     */
    protected $refreshToken;
    /**
     * @var int Determine whether or not to return the user data as an array (default) or an object.
     */
    protected $returnType   = self::RETURN_ARRAY;
    /**
     * @var string The URL to retrieve the OAuth token.
     */
    protected $tokenUrl     = 'https://oauth.itservices.msu.edu/oauth/token';
    /**
     * @var string The class to instantiate if the return type is set to RETURN_OBJECT
     */
    protected $userClass = '\Plc\User\Security\OAuth2\User\User';


    public function __construct($clientId = null, $clientSecret = null, $redirectUrl = null)
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUrl  = $redirectUrl;
    }

    /**
     * A wrapper to provide simple, turn-key authentication.
     *
     * @return bool True if the user is authenticated, false otherwise.
     */
    public function authenticate()
    {
        // Check for an access token and if one does not exist then request one.
        // If an access token is present in the query string then attempt
        // authentication.
        if (false === isset($_GET['code'])) {

            $this->fetchAuthorizationCode();

        } else {

            $authResult = false;
            $response   = $this->fetchTokens();

            if ($response['httpStatusCode'] == 200) {
                 $authResult = true;
            }

            return $authResult;
        }
    }


    /**
     * Sends the user to MSU's OAuth authorization server so the user can sign
     * in. The user will be redirected back to your application with an
     * authorization code in the query string.
     */
    public function fetchAuthorizationCode()
    {
        $query = array(
            'client_id'     => $this->clientId,
            'redirect_uri'  => $this->redirectUrl,
            'response_type' => 'code',
        );

        header('Location: ' . $this->authUrl . '?' . http_build_query($query, null, '&'));
        die();
    }


    /**
     * Retrieve access and refresh tokens from MSU's OAuth token server.
     *
     * Sends a POST request via Curl and returns an array with token data,
     * including the HTTP return status. This must be performed after
     * fetchAuthorizationCode().
     *
     * @return array $returnData An array of tokens and related data.
     */
    public function fetchTokens()
    {
        $query = array(
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'code'          => $_GET['code'],
            'grant_type'    => 'authorization_code',
            'redirect_uri'  => $this->redirectUrl,
        );

        $curlOptions = array(
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => http_build_query($query, null, '&'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_URL            => $this->tokenUrl,
        );

        $curl = curl_init();
        curl_setopt_array($curl, $curlOptions);
        $curlResult = json_decode(curl_exec($curl));

        $this->accessToken  = $curlResult->access_token;
        $this->refreshToken = $curlResult->refresh_token;

        $returnData = array(
            'accessToken'    => $curlResult->access_token,
            'expiresIn'      => $curlResult->expires_in,
            'httpStatusCode' => curl_getinfo($curl, CURLINFO_HTTP_CODE),
            'refreshToken'   => $curlResult->refresh_token,
            'tokenType'      => $curlResult->token_type,
        );

        return $returnData;
    }


    /**
     * Returns the member variable $accessToken;
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }


    /**
     * Returns the member variable $authUrl;
     * @return string
     */
    public function getAuthUrl()
    {
        return $this->authUrl;
    }


    /**
     * Returns the member variable $clientId;
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }


    /**
     * Returns the member variable $clientSecret;
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }


    /**
     * Returns the member variable $dataUrl;
     * @return string
     */
    public function getDataUrl()
    {
        return $this->dataUrl;
    }


    /**
     * Returns the member variable $redirectUrl;
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Returns the member variable $refreshToken;
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }


    /**
     * Returns the member variable $tokenUrl;
     * @return string
     */
    public function getTokenUrl()
    {
        return $this->tokenUrl;
    }


    /**
     * Returns an array of information about the authenticated user.
     *
     * @return array
     */
    public function getUserData()
    {
        $curlOptions = array(
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_URL            => $this->dataUrl . '?access_token=' . $this->accessToken,
        );

        $curl       = curl_init();
        curl_setopt_array($curl, $curlOptions);
        $curlResult = json_decode(curl_exec($curl));

        $userData = array(
            'email' => $curlResult->info->email,
            'fName' => $curlResult->info->first_name,
            'lName' => $curlResult->info->last_name,
            'netId' => $curlResult->info->msunetid,
            'uuId'  => $curlResult->uid,
        );

        if (self::RETURN_OBJECT === $this->returnType) {
            $user = new $this->userClass;
            $user->mapFromArray($userData);
            return $user;
        }

        return $userData;
    }


    /**
     * Sets the member variable $authUrl;
     * @return string
     */
    public function setAuthUrl($authUrl)
    {
        $this->authUrl = $authUrl;
        return $this;
    }


    /**
     * Sets the member variable $clientId;
     * @return string
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }


    /**
     * Sets the member variable $clientSecret;
     * @return string
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }


    /**
     * Sets the member variable $dataUrl;
     * @return string
     */
    public function setDataUrl($dataUrl)
    {
        $this->dataUrl = $dataUrl;
        return $this;
    }


    /**
     * A helper method to configure the URLs based on the development tier.
     *
     * @param  int $environment
     * @return \Plc\User\Security\Sentinel\Sentinel
     * @throws Exception
     */
    public function setEnvironment($environment)
    {
        if (self::ENV_DEV === $environment) {
            $this->authUrl  = 'https://oauth.qual.ais.msu.edu/oauth/authorize';
            $this->dataUrl  = 'https://oauth.qual.ais.msu.edu/oauth/me';
            $this->tokenUrl = 'https://oauth.qual.ais.msu.edu/oauth/token';

        } elseif (self::ENV_PROD === $environment) {
            $this->authUrl  = 'https://oauth.ais.msu.edu/oauth/authorize';
            $this->dataUrl  = 'https://oauth.ais.msu.edu/oauth/me';
            $this->tokenUrl = 'https://oauth.ais.msu.edu/oauth/token';


        } else {
            throw new Exception('Unrecognized environment: ' . $environment);
        }

        return $this;
    }


    /**
     * Sets the member variable $redirectUrl;
     * @return string
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }


    /**
     * Sets the member variable $returnType.
     *
     * @param  $returnType
     */
    public function setReturnType($returnType)
    {
        $returnTypes = array(
            self::RETURN_ARRAY,
            self::RETURN_OBJECT
        );

        if (false === in_array($returnType, $returnTypes)) {
            throw new \Exception('Invalid return type');
        }

        $this->returnType = $returnType;

        return $this;
    }


    /**
     * Sets the member variable $tokenUrl;
     * @return string
     */
    public function setTokenUrl($tokenUrl)
    {
        $this->tokenUrl = $tokenUrl;
        return $this;
    }


    /**
     * Set the user class to use when RETURN_OBJECT is specified as the return
     * type.
     *
     * @param  string $userClass
     * @throws \Exception
     */
    public function setUserClass($userClass)
    {
        $testClass = new $userClass;

        if (false === $testClass instanceof UserInterface) {
            throw new \Exception('User class "' . $userClass . '" does not implement Plc\User\Security\OAuth2\User\UserInterface');
        }

        $this->userClass = $userClass;
    }
}
