<?php

namespace Plc\User;

abstract class UserAbstract
{
    /**
     * @var string The user's email.
     */
    protected $email;
    /**
     * @var string The user's first name.
     */
    protected $fName;
    /**
     * @var string The user's lname.
     */
    protected $lName;
    /**
     * @var string The user's MSU NetID.
     */
    protected $netId;


    /**
     * Returns the member variable $email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Returns the member variable $fName
     *
     * @return string
     */
    public function getFName()
    {
        return $this->fName;
    }


    /**
     * Returns the member variable $lName
     *
     * @return string
     */
    public function getLName()
    {
        return $this->lName;
    }


    /**
     * Returns the member variable $netId
     *
     * @return string
     */
    public function getNetId()
    {
        return $this->netId;
    }


    /**
     * Populate the member variables from an array.
     *
     * @param array $data
     */
    public function mapFromArray($data)
    {
            foreach ($data as $classVar => $value) {
                $this->$classVar = $value;
            }
    }


    /**
     * Sets the member variable $email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
         $this->email = $email;
    }


    /**
     * Sets the member variable $fName
     *
     * @param string $fName
     */
    public function setFName($fName)
    {
         $this->fName = $fName;
    }


    /**
     * Sets the member variable $lName
     *
     * @param string $lName
     */
    public function setLName($lName)
    {
         $this->lName = $lName;
    }


    /**
     * Sets the member variable $netId
     *
     * @param string $netId
     */
    public function setNetId($netId)
    {
         $this->netId = $netId;
    }


    /**
     * Returns the member variables as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'email'             => $this->email,
            'fName'             => $this->fName,
            'lName'             => $this->lName,
            'netId'             => $this->netId,
        );
    }


}