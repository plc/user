<?php

namespace Plc\User\Ldap;

use Plc\User\Ldap\Exception\CouldNotConnectException;
use Plc\User\Ldap\User\UserInterface;

class LdapQuery
{
    const RETURN_ARRAY  = 0;
    const RETURN_OBJECT = 1;

    /**
     * @var resource The resource created by ldap_connect()
     */
    protected $connection;
    /**
     * @var array An array of filters to limit the scope of the search. Populated by filterByX() methods.
     */
    protected $filter = array();
    /**
     * @var int Determines whether or not the search results should be a multidimensional array or an array of objects
     */
    protected $returnType = LdapQuery::RETURN_ARRAY;
    /**
     * @var string The MSU LDAP server to connect to
     */
    protected $server     = 'directory.mail.msu.edu';
    /**
     * @var string The root of the directory to search at
     */
    protected $searchRoot = 'dc=msu, dc=edu';
    /**
     * @var string The class to instantiate if the return typ is set to RETURN_OBJECT
     */
    protected $userClass = '\Plc\User\Ldap\User\User';


    /**
     * Construct and configure the class.
     *
     * @param string $server
     * @param string $searchRoot
     * @param string $returnType
     */
    public function construct($server = null, $searchRoot = null, $returnType = LdapQuery::RETURN_ARRAY)
    {
        $this->returnType = is_null($returnType) ? $this->returnType : $returnType;
        $this->server     = is_null($server)     ? $this->server     : $server;
        $this->searchRoot = is_null($searchRoot) ? $this->searchRoot : $searchRoot;
    }


    /**
     * Create a connection to the LDAP server.
     *
     * @throws CouldNotConnectException
     */
    public function connect()
    {
        $this->connection = ldap_connect($this->server);

        if (false === $this->connection) {
            throw new CouldNotConnectException('Could not connect to LDAP server.');
        }
    }


    /**
     * Returns the member variable $connection.
     *
     * @return resource
     */
    public function getConnection()
    {
        return $this->connection;
    }


    /**
     * Return the member variable $returnType.
     *
     * @return int
     */
    public function getReturnType()
    {
        return $this->returnType;
    }

    /**
     * Returns the member variable $searchRoot.
     *
     * @return string
     */
    public function getSearchRoot()
    {
        return $searchRoot;
    }


    /**
     * Returns the member variable $server.
     *
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Return the member variable $userClass
     *
     * @return string
     */
    public function getUserClass()
    {
        return $this->userClass;
    }


    /**
     * A helper method to construct the filter string used by ldap_search().
     *
     * @param string $filterKey
     * @param string $ldapKey
     * @param string $value
     */
    protected function filterBy($filterKey, $ldapKey, $value)
    {
        $this->filter[$filterKey] = '(' . $ldapKey . '=' . $value . ')';
    }


    /**
     * Add a condition to search based on city.
     *
     * @param  string $city
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByCity($city)
    {
        $this->filterBy('city', 'l', $city);
        return $this;
    }


    /**
     * Add a condition to search based on email address.
     *
     * @param  string $email
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByEmail($email)
    {
        $this->filterBy('email', 'mail', $email);
        return $this;
    }


    /**
     * Add a condition to search based on employee type.
     *
     * @param  string $employeeType
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByEmployeeType($employeeType)
    {
        $this->filterBy('employeeType', 'employeetype', $employeeType);
        return $this;
    }


    /**
     * Add a condition to search based on department.
     *
     * @param  string $department
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByDepartment($department)
    {
        $this->filterBy('department', 'departmentnumber', $department);
        return $this;
    }

    /**
     * Add a condition to search based on first name.
     * @param  string $fName
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByFName($fName)
    {
        $this->filterBy('fName', 'givenname', $fName);
        return $this;
    }


    /**
     * Add a condition to search based on last name.
     *
     * @param  string $lName
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByLName($lName)
    {
        $this->filterBy('lName', 'sn', $lName);
        return $this;
    }


    /**
     * Add a condition to search based on NetID.
     *
     * @param  string $netId
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByNetId($netId)
    {
        $this->filterBy('netId', 'uid', $netId);
        return $this;
    }


    /**
     * Add a condition to search based on PO box.
     *
     * @param  string $poBox
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByPoBox($poBox)
    {
        $this->filterBy('poBox', 'postofficebox', $poBox);
        return $this;
    }


    /**
     * Add a condition to search based on postal code.
     *
     * @param  string $postalCode
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByPostalCode($postalCode)
    {
        $this->filterBy('postalCode', 'postalcode', $postalCode);
        return $this;
    }


    /**
     * Add a condition to search based on phone number.
     *
     * @param  string $phoneNumber
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByPhoneNumber($phoneNumber)
    {
        $this->filterBy('phoneNumber', 'telephonenumber', $phoneNumber);
        return $this;
    }


    /**
     * Add a condition to search based on state.
     *
     * @param  string $state
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByState($state)
    {
        $this->filterBy('state', 'st', $state);
        return $this;
    }


    /**
     * Add a condition to search based on street address.
     *
     * @param  string $street
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByStreet($street)
    {
        $this->filterBy('street', 'street', $street);
        return $this;
    }


    /**
     * Add a condition to search based on title.
     *
     * @param  string $title
     * @return \Plc\User\Ldap\LdapQuery
     */
    public function filterByTitle($title)
    {
        $this->filterBy('title', 'title', $title);
        return $this;
    }


    /**
     * Search the LDAP directory based on predefined filters. Returns an array
     * of data or false if no records were found.
     *
     * @return mixed
     */
    public function find()
    {
        if (null === $this->connection) {
            $this->connect();
        }

        if (0 === count($this->filter)) {
            echo 'filter not set';
            return;
        }

        $filter  = '(&' . implode('', $this->filter) . ')';
        $results = ldap_search($this->connection, $this->searchRoot, $filter);

        if (0 === $results) {
            return false;
        }

        return $this->parseResults($results);
    }


    /**
     * Returns a single search result or false if no records were found.
     *
     * @return mixed
     */
    public function findOne()
    {
        $result = $this->find();

        return is_array($result) ? $result[0] : $result;
    }


    /**
     * Searches the array for a given field. If found, it returns it, otherwise
     * null is returned.
     *
     * @param  array $data
     * @param  string $key
     * @return mixed
     */
    protected function parseRecord($data, $key)
    {
        return key_exists($key, $data) ? $data[$key][0] : null;
    }


    /**
     * Receives a resource result from ldap_search(), processes the search
     * results and returns an array of data.
     *
     * @param  Resource $resultResource
     * @return type
     */
    protected function parseResults($resultResource)
    {
        $returnResults = array();
        $count         = ldap_count_entries($this->connection, $resultResource);
        $searchResults = ldap_get_entries($this->connection, $resultResource);

        while ($count--) {
            $record = $searchResults[$count];
            $data   = array(
                'addressCity'       => $this->parseRecord($record, 'l'),
                'addressState'      => $this->parseRecord($record, 'st'),
                'addressStreet'     => $this->parseRecord($record, 'street'),
                'addressPoBox'      => $this->parseRecord($record, 'postofficebox'),
                'addressPostalCode' => $this->parseRecord($record, 'postalcode'),
                'department'        => $this->parseRecord($record, 'departmentnumber'),
                'employeeType'      => $this->parseRecord($record, 'employeetype'),
                'fName'             => $this->parseRecord($record, 'givenname'),
                'lName'             => $this->parseRecord($record, 'sn'),
                'email'             => $this->parseRecord($record, 'mail'),
                'netId'             => $this->parseRecord($record, 'uid'),
                'phoneNumber'       => $this->parseRecord($record, 'telephonenumber'),
                'professionalName'  => $this->parseRecord($record, 'cn'),
                'title'             => $this->parseRecord($record, 'title'),
            );

            if ($this->returnType === self::RETURN_ARRAY) {
                $returnResults[] = $data;
            } else {
                $user = new $this->userClass;
                $user->mapFromArray($data);
                $returnResults[] = $user;
            }
        }

        return $returnResults;
    }


    /**
     * Sets the member variable $connection.
     *
     * @param  string $connection
     * @throws \InvalidArgumentException
     */
    public function setConnection($connection)
    {
        if (false === is_resource($connection)) {
            throw new \InvalidArgumentException('The passed parameter is not a resource.');
        }

        $this->connection = $connection;
    }


    /**
     * Sets the member variable $returnType.
     *
     * @param  $returnType
     */
    public function setReturnType($returnType)
    {
        $returnTypes = array(
            self::RETURN_ARRAY,
            self::RETURN_OBJECT
        );

        if (false === in_array($returnType, $returnTypes)) {
            throw new \Exception('Invalid return type');
        }

        $this->returnType = $returnType;

        return $this;
    }


    /**
     * Sets the member variable $searchRoot.
     *
     * @param string $searchRoot
     */
    public function setSearchRoot($searchRoot)
    {
        $this->searchRoot = $searchRoot;
    }


    /**
     * Sets the member variable $server.
     *
     * @param string $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }


    /**
     * Set the user class to use when RETURN_OBJECT is specified as the return
     * type.
     *
     * @param  string $userClass
     * @throws \Exception
     */
    public function setUserClass($userClass)
    {
        $testClass = new $userClass;

        if (false === $testClass instanceof UserInterface) {
            throw new \Exception('User class "' . $userClass . '" does not implement Plc\User\UserInterface');
        }

        $this->userClass = $userClass;
    }
}