# LDAP
---
## Filters

The *LdapQuery* class includes several filter methods that can be used to refine the query made to the MSU Directory server. The values may be specific (e.g., 'sparty' returns only 'sparty') or include an asterisk as a wildcard (e.g., 'spart*' returns both 'sparty' and 'spartan'). Additionally, the values may be case insensitive. All values that are returned may be filtered.

Example:

```php
<?php

use Plc\User\Ldap\LdapQuery;

$ldap    = new LdapQuery();
$results = $ldap->filterByEmployeeType('staff')->filterByNetId('spart*')->find();
```

## Executing the query

After constructing your query with filter methods, you need to execute the query with either ```find()``` or ```findOne()```. ```find()``` will return an array of results, where as ```findOne()``` will just return one single element. If no records are found, then ```false``` will be returned.

### Return types

For each record requested, *LdapQuery* will default to returning an array. By executing ```setReturnType()``` an object of the type *Plc\User\Ldap\User\UserInterface* will be constructed. By default, *Plc\User\Ldap\User\User* is the class that will be instantiated, however this can be overridden by using ```setUserClass()``` and providing a fully qualified namespaced class.

```
<?php

use Plc\User\Ldap\LdapQuery;

$ldap  = new LdapQuery();
// Instantiates and returns a single \Foo\Bar\User object, which implements \Plc\User\Ldap\User\UserInterface.
$user = $ldap->setReturnType(LdapQuery::RETURN_OBJECT)->setUserClass('\Foo\Bar\User')->filterByNetId('sparty')->findOne();
```