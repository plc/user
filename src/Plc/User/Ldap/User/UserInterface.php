<?php

namespace Plc\User\Ldap\User;

interface UserInterface
{
    public function getAddressCity();
    public function getAddressPostalCode();
    public function getAddressState();
    public function getAddressStreet();
    public function getDepartment();
    public function getEmail();
    public function getEmployeeType();
    public function getFName();
    public function getLName();
    public function getNetId();
    public function getPhoneNumber();
    public function getTitle();
    public function mapFromArray($data);
    public function setAddressCity($addressCity);
    public function setAddressPostalCode($addressPostalCode);
    public function setAddressState($addressState);
    public function setAddressStreet($addressStreet);
    public function setDepartment($department);
    public function setEmail($email);
    public function setEmployeeType($employeeType);
    public function setFName($fName);
    public function setLName($lName);
    public function setNetId($netId);
    public function setPhoneNumber($phoneNumber);
    public function setTitle($title);

}