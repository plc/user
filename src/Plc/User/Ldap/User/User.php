<?php

namespace Plc\User\Ldap\User;

use Plc\User\UserAbstract;
use Plc\User\Ldap\User\UserInterface;

class User extends UserAbstract implements UserInterface
{
    /**
     * @var string The city of the user's address
     */
    protected $addressCity;
    /**
     * @var string The postal code of the user's address
     */
    protected $addressPostalCode;
    /**
     * @var string The state of the user's address
     */
    protected $addressState;
    /**
     * @var string The street name and number of the user's address
     */
    protected $addressStreet;
    /**
     * @var string The user's department.
     */
    protected $department;
    /**
     * @var string The type of employee.
     */
    protected $employeeType;
    /**
     * @var string The user's phone number.
     */
    protected $phoneNumber;
    /**
     * @var string The user's professional name.
     */
    protected $professionalName;
    /**
     * @var string The user's title.
     */
    protected $title;


    /**
     * Returns the member variable $addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }


    /**
     * Returns the member variable $addressPostalCode
     *
     * @return string
     */
    public function getAddressPostalCode()
    {
        return $this->addressPostalCode;
    }


    /**
     * Returns the member variable $addressState
     *
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }


    /**
     * Returns the member variable $addressStreet
     *
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }


    /**
     * Returns the member variable $addressDepartment
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }


    /**
     * Returns the member variable $email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Returns the member variable $employeeType
     *
     * @return string
     */
    public function getEmployeeType()
    {
        return $this->employeeType;
    }


    /**
     * Returns the member variable $fName
     *
     * @return string
     */
    public function getFName()
    {
        return $this->fName;
    }


    /**
     * Returns the member variable $lName
     *
     * @return string
     */
    public function getLName()
    {
        return $this->lName;
    }


    /**
     * Returns the member variable $netId
     *
     * @return string
     */
    public function getNetId()
    {
        return $this->netId;
    }


    /**
     * Returns the member variable $phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }


    /**
     * Returns the member variable $professionalName
     *
     * @return string
     */
    public function getProfessionalName()
    {
        return $this->professionalName;
    }


    /**
     * Returns the member variable $title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Sets the member variable $addressCity
     *
     * @param string $addressCity
     */
    public function setAddressCity($addressCity)
    {
         $this->addressCity = $addressCity;
    }


    /**
     * Sets the member variable $addressPostalCode
     *
     * @param string $addressPostalCode
     */
    public function setAddressPostalCode($addressPostalCode)
    {
         $this->addressPostalCode = $addressPostalCode;
    }


    /**
     * Sets the member variable $addressState
     *
     * @param string $addressState
     */
    public function setAddressState($addressState)
    {
         $this->addressState = $addressState;
    }


    /**
     * Sets the member variable $addressStreet
     *
     * @param string $addressStreet
     */
    public function setAddressStreet($addressStreet)
    {
         $this->addressStreet = $addressStreet;
    }


    /**
     * Sets the member variable $department
     *
     * @param string $department
     */
    public function setDepartment($department)
    {
         $this->department = $department;
    }


    /**
     * Sets the member variable $email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
         $this->email = $email;
    }


    /**
     * Sets the member variable $employeeType
     *
     * @param string $employeeType
     */
    public function setEmployeeType($employeeType)
    {
         $this->employeeType = $employeeType;
    }


    /**
     * Sets the member variable $fName
     *
     * @param string $fName
     */
    public function setFName($fName)
    {
         $this->fName = $fName;
    }


    /**
     * Sets the member variable $lName
     *
     * @param string $lName
     */
    public function setLName($lName)
    {
         $this->lName = $lName;
    }


    /**
     * Sets the member variable $netId
     *
     * @param string $netId
     */
    public function setNetId($netId)
    {
         $this->netId = $netId;
    }


    /**
     * Sets the member variable $phoneNumber
     *
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
         $this->phoneNumber = $phoneNumber;
    }


    /**
     * Sets the member variable $professionalName
     *
     * @param string $professionalName
     */
    public function setProfessionalName($professionalName)
    {
         $this->professionalName = $professionalName;
    }


    /**
     * Sets the member variable $title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
         $this->title = $title;
    }


    /**
     * Returns the member variables as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'addressCity'       => $this->addressCity,
            'addressPostalCode' => $this->addressPostalCode,
            'addressState'      => $this->addressState,
            'addressStreet'     => $this->addressStreet,
            'department'        => $this->department,
            'email'             => $this->email,
            'employeeType'      => $this->employeeType,
            'fName'             => $this->fName,
            'lName'             => $this->lName,
            'netId'             => $this->netId,
            'phoneNumber'       => $this->phoneNumber,
            'professionalName'  => $this->professionalName,
            'title'             => $this->title,
        );
    }


}