# MSU User
---
A library to work with MSU user based systems.  It is [PSR-0](http://www.php-fig.org/psr/0/), [PSR-1](http://www.php-fig.org/psr/1/) and [PSR-2](http://www.php-fig.org/psr/2/) compliant.

#### [LDAP](https://gitlab.msu.edu/plc/user/blob/develop/src/Plc/User/Ldap/README.md)
Query the MSU directory for user information.

#### [OAuth 2](https://gitlab.msu.edu/plc/user/blob/develop/src/Plc/User/Security/OAuth2/README.md)
Authenticate MSU NetIDs via the OAuth 2 protocol and retrieve user information.

#### [Sentinel](https://gitlab.msu.edu/plc/user/tree/develop/src/Plc/User/Security/Sentinel)
Authenticate MSU NetIDs via the Sentinel service and retrieve user information.

## How to install

MSU User is best used as a Composer package. Include the following in your composer.json file:

```json

    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.msu.edu:plc/user.git"
        }
    ],

    "require": {
        "plc/user" : "^1.0"
    }
```

Then run ```composer install```.

## Contribution guide

If you wish to contribute to this project, please adhere to the Contribution Guidelines: https://gitlab.msu.edu/plc/user/wikis/contribute-index

## Additional documentation

Doxygen generated API documentation may be found in doc/api/html/index.html.